# Set the base image to libscapi base
FROM scapicryptobiu/libscapi

RUN apt update
RUN apt-get install -y openssh-server sudo

WORKDIR /root

# ssh
RUN sed -ri 's/UsePAM yes/UsePAM no/g' /etc/ssh/sshd_config
RUN mkdir -p /var/run/sshd && chmod 755 /var/run/sshd
ADD id_rsa.pub /root/.ssh/authorized_keys
RUN chmod -R g-rwx,o-rwx /root/.ssh

# Clean
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* /root/boost_1_60_0.tar.bz2

# Add a normal user
RUN useradd -p tcuser -G sudo -s /bin/bash -d /work/tf0054 tf0054
RUN ln -s /work/tf0054 /home/tf0054

# modify sudoers
# Enable passwordless sudo for users under the "sudo" group 
RUN sed -i.bkp -e \
    's/%sudo\s\+ALL=(ALL\(:ALL\)\?)\s\+ALL/%sudo ALL=NOPASSWD:ALL/g' \
    /etc/sudoers

# Exec
EXPOSE 22
ENTRYPOINT [ "/usr/sbin/sshd", "-D" ]
